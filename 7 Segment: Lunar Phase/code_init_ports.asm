	;; INITIALIZE PORTs as output
	;;
	;; 7 segments uses PB{4..7},PC{7},PD{6,7},PE{6} as INPUT
	;;
	;; SRAM uses PB{1,2,3},PC{6},PD{0..4},PF{0,1,4..7}
	;; of those :
	;; 	PB{1,2,3} and PD{0..4} are both INPUT and OUTPUT
	;; 	(these are for data)
	;; whereas :
	;; 	PC{6}, PF{0,1,4..7} are only INPUT
	;; 	(PC{6} is Write Enable, and the rest are for addressing)
	;; 
	;; Thus, i'll initialise all as OUTPUT on the board to save cycles
	;; In total :
	;; 	PB{1..7}, PC{6,7}, PD{0..4,6,7}, PE{6}, PF{0,1,4..7}
	;; 
	
	;; PB{1..7}
	;; 
	lds tmp, DDRB		; Data Direction Register 
	ori tmp, 0xFE		; set as 1s
	out DDRB, tmp

	lds tmp, PINB		; Port Input Pin
	andi tmp, 1		; set as 0s
	out PINB, tmp		;
	;; PC{6,7}
	;; 
	sbi DDRC, 6
	sbi DDRC, 7		;
	cbi PINC, 6
	cbi PINC, 7		;
	;; PD{0..4,6,7}
	;; 
	lds tmp, DDRD		;
	ori tmp, 0xDF		; set as 1s
	out DDRD, tmp		;

	lds tmp, PIND		; 
	andi tmp, 0x20		; set as 0s
	out PIND, tmp		;
	;; PE{6}
	;; 
	sbi DDRE, 6		;
	cbi PINE, 6		;
	;; PF{0,1,4..7}
	;; 
	lds tmp, DDRF
	ori tmp, 0xFB
	out DDRF, tmp

	lds tmp, PINF
	andi tmp, 0x04
	out PINF, tmp
	;;
	;; INITIALIZE_END
