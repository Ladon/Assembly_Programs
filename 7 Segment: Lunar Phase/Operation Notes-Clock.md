the addresses used are of the form **0x1FF?** so 4 bits are enough

the data though, needs 8 bits.

Thus, at least **12** bits are needed, excluding E1,E2,W,G

### READ mode :
 * W  = 1
 * E1 = 0
 * E2 = 1
 * G  = 1 (probably)
 
### Write mode :
 * W  = 0
 * E1 = 0
 * E2 = 1
 * G  = 1

**TEST** to see if we can put **E1** to GND and **E2**,**G** to VCC, so that we use
13 bits in total

### Arduino to SRAM Pin Mappings

>               +-----+
>      (NC)  FT |ο    | Vcc  (5V)
>      (5V) A12 |     |   W (PC6)
>      (5V)  A7 |     |  E2  (5V)
>      (5V)  A6 |     |  Α8  (5V)
>     (PF7)  A5 |     |  A9  (5V)
>     (PF6)  A4 |     | A11  (5V)
>     (PF5)  A3 |     |   G  (5V)
>     (PF4)  A2 |     | A10  (5V)
>     (PF1)  A1 |     |  E1 (GND)
>     (PF0)  A0 |     | DQ7 (PB3)
>     (PD0) DQ0 |     | DQ6 (PB2)
>     (PD1) DQ1 |     | DQ5 (PB1)
>     (PD2) DQ2 |     | DQ4 (PD4)
>     (GND) Vss |     | DQ3 (PD3)
>               +-----+
