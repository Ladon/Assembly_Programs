## pin functions from the pinout by [bq](http://www.pighixxx.net/)
---
* pin ***13*** :
    * physical pin : ***32***
    * **PC7** : Port C bit 7
    * **ICP3** : Input Capture
    * **CLK0** : Microcontroller Working Frequency output
	* controlled by CKOUT fuse
	    * set to 1 to disable
	    * defaults to 1
	        * but fuses stay when offline
    * **OC4A** : PWM from Timer 4
        * controlled by TCCR4A register
	    * set bits 7,6 to 0 to disable
	    * defaults to 0,0
    * onboard LED
---
* pin ***12*** :
    * physical pin : ***26***
    * **PD6** : Port D bit 6
    * **T1** : Timer/Counter1 counter source
    * **OC4D** : PWM from Timer 4
        * controlled by TCCR4C register
	    * set bits 3,2 to 0 to disable
	    * defaults to 0,0
    * **ADC9** : Analog to Digital Converter, Channel 9
        * controlled by DIDR2 register
	    * set bit 1 to 0 to disable
	    * defaults to 0
---
* pin ***11*** :
    * physical pin : ***12***
    * **PB7** : Port B bit 7
    * **PCINT7** : Pin Change Interrupt 7
        * controlled by PCMSK0 register
	     * set bit 7 to 0 to disable
	     * defaults to 0
    * **OC1C** : Output Compare and PWM Output C for Timer/Counter1
    * **OC0A** : Output Compare and PWM Output A for Timer/Counter0
    * **RTS** : UART flow control RTS signal
---
* pin ***10*** :
    * physical pin : ***30***
    * **PB6**
    * **PCINT6**
    * **OC1B**
    * **OC4B**
    * **ADC13**
---
* pin ***9*** :
    * physical pin : ***29***
    * **PB5**
    * **PCINT5**
    * **OC1A**
    * **OC4B**
    * **ADC12**
---
* pin ***8*** :
    * physical pin : ***28***
    * **PB4**
    * **PCINT4**
    * **ADC11**
---
* pin ***7*** :
    * physical pin : ***1***
    * **PE6**
    * **AIN0** : Analog Comparator Negative input.
    * **INT.6** : External Interrupt 6 Input
---
* pin ***6*** :
    physical pin : ***27***
    * **PD7**
    * **T0** : Timer 0 Clock Input
    * **OC4D**
    * **ADC10**
---
> ![leonardo pinout image](images/leonardo.png)]
## pin enabling sequence

in total, we need pins PB7,6,5,4, PC7, PD7,6, PE6

* to configure them as outputs, we need :
    * DDRxn -> 1, where 'x' is the port and 'n' the bit number
        * (DDR := Data Direction Register)
    * PINxn -> 0
        * (PIN := Port Input Pin, toggles PORTxn when 1, disregarding DDRxn)
