#!/bin/bash
stty -F /dev/ttyACM0 9600 &&
# avrdude -p m32u4 -c avr109 -b 115200 -P /dev/ttyACM0 -U flash:w:test.hex -U lfuse:w:0xFF:m
avrdude -p m32u4 -c avr109 -b 9600 -P /dev/ttyACM0 -U efuse:r:-:b -U hfuse:r:-:b -U lfuse:r:-:b -U flash:w:blink.hex
