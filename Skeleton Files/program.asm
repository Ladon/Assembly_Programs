	.DEVICE	ATmega32U4	; needed by the assembler (gavrasm)

	;; DEFINE aliases
	;;
	.DEF tmp=r16		; define r16 as temporary space
	.DEF tmp2=r17
	.DEF param=r18		; used for SUBROUTINES
	.DEF XL=r26
	.DEF XH=r27
	.DEF YL=r28
	.DEF YH=r29
	.DEF ZL=r30
	.DEF ZH=r31
	;;
	;; DEFINE_END

	;; REDUCE clock speed
	;;
	.include code_reduce_clk.asm
	;;
	;; REDUCE_END
	
	;; MAIN
	;; 
	;;
	;; MAIN_END

	;; SUBROUTINE DELAY
	;;
delay:
	clr tmp2		; 1 clock
	;; outer loop begin
dloop2:	
	clr tmp			; 1 * dloop2(255)
	;; inner loop begin
dloop:	
	inc tmp			; 255 * dloop2(255)
	cpi tmp,255		; 255 * dloop2(255)
	brne dloop		; (254*2 + 1) * dloop2(255)
	;; inner loop end
	inc tmp2		; 255
	cpi tmp2,255		; 255
	brne dloop2		; 254 * 2 + 1
	;; outer loop end	
	ret			; 4
	;;
	;; SUBROUTINE_END	; total of 261124 cycles ~ 4s at 65KHz
