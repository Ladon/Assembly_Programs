common cathode (CC)
crude estimation : 750 Ohm @ 5V = ~7mA

#### arduino pins to LEDs :

    13 (PC7) o   11 (PB7)
                 -------
               |         |
      12 (PD6) |         | 10 (PB6)
               | 8 (PB4) |
                 -------
               |         |
       6 (PD7) |         | 9 (PB5)
               | 7 (PE6) |
                 -------        

    there is a common GND next to the dot and right accross
    it on the other side 
